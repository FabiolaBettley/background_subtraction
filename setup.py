from setuptools import setup

setup(name='background_subtraction',
      version='1.0',
      description='Removes background in a given image',
      url='https://bitbucket.org/architech/background_subtraction/overview',
      author='Jawad Ateeq',
      author_email='jateeq@architech.ca',
      license='',
      packages=['background_subtraction'],
      zip_safe=False)
