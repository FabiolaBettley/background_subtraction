#import the necessary packages
import argparse
import datetime
import time
import cv2
import pdb
import numpy as np

def createFrame(name, width, height, X, Y ):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)	
    cv2.resizeWindow(name, width, height)
    cv2.moveWindow(name, X, Y)

def subtract_background(img, filter_area, detect_shadows=False):
    #create an instance of the MOG2 background subtractor
    bg_mog2 = cv2.createBackgroundSubtractorMOG2(detect_shadows)
    
    frame_mog2 = bg_mog2.apply(frame)

def main():
    #construct arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", help="Path to video file")
    args = vars(ap.parse_args())

    #get the video from the argument, and open it
    vid = args['video']

    if vid is None:
        cap = cv2.VideoCapture(0)
        time.sleep(0.25)
        print 'No video found'
    else:
        cap = cv2.VideoCapture(vid)

    #create display windows
    createFrame("Original", 500, 500, 50, 10)
    createFrame("Frame_MOG2", 500, 500, 600, 10)
    createFrame("Eroded", 500, 500, 1125,35)
    createFrame("Opened", 500, 500, 50, 600)
    createFrame("Closed", 500, 500, 600, 600)
    createFrame("Eroded_elliptical_kernel", 500, 500, 1125, 600)

    #holds all the tracking dots
    circle_pos = []

    #create an instance of the MOG2 background subtractor
    bg_mog2 = cv2.createBackgroundSubtractorMOG2(detectShadows=False)

    while True:
        (status, frame) = cap.read()
        
        if status == True:

            #apply the gaussian mixture model given by MOG2 in OpenCV
            frame_mog2 = bg_mog2.apply(frame)

            #apply a binary threshold to convert all pixels to white
            frame_threshold = cv2.threshold(frame_mog2, 25, 255, cv2.THRESH_BINARY)[1]        
            #create a rectangular kernel
            kernel = np.ones((2,2), np.uint8)

            #create an eroded frame            
            eroded = cv2.erode(frame_mog2, kernel, iterations = 1)
            
            #create an opened frame
            opened = cv2.morphologyEx(frame_mog2, cv2.MORPH_CLOSE, kernel)

            #create a closed frame            
            closed = cv2.morphologyEx(frame_mog2, cv2.MORPH_OPEN, kernel)
    
            #create an eroded frame with ellptical kernel
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
            eroded_elliptical_kernel = cv2.erode(frame_mog2, kernel, iterations = 1)
           

            #Draw the frames
            cv2.imshow("Original", frame)
            cv2.imshow("Frame_MOG2", frame_threshold)
            cv2.imshow("Eroded", eroded)
            cv2.imshow("Opened", opened)
            cv2.imshow("Closed", closed)
            cv2.imshow("Eroded_elliptical_kernel", eroded_elliptical_kernel)

            #opencv doesn't show the frame unless the following line is added
            key = cv2.waitKey(33) & 0xFF

            #if the 'q' key is pressed, break from the loop
            if key == ord("q"):
                break

        else:
            print 'invalid frame\n'
            break

    cv2. destroyAllWindows()

if __name__ == "__main__":
    main()
