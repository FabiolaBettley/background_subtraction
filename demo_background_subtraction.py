#import the necessary packages
import argparse
import datetime
import time
import cv2
import pdb
import numpy as np
from background_subtraction import background_subtractor

def createFrame(name, width, height, X, Y ):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)	
    cv2.resizeWindow(name, width, height)
    cv2.moveWindow(name, X, Y)

def main():
    #construct arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", help="Path to video file")
    args = vars(ap.parse_args())

    #get the video from the argument, and open it
    vid = args['video']
    print 'Vid path: ', vid

    if vid is None:
        cap = cv2.VideoCapture(0)
        time.sleep(0.25)
        print 'No video found'
    else:
        cap = cv2.VideoCapture(vid)

    #create display windows
    createFrame("Original", 500, 500, 50, 10)
    createFrame("Background Sub", 500, 500, 600, 10)  
    
    bg_sub = background_subtractor()

    while True:
        (status, frame) = cap.read()
        
        if status == True:            
            frame_bgsub = bg_sub.removeBackground(frame)

            #Draw the frames
            cv2.imshow("Original", frame)
            cv2.imshow("Background Sub", frame_bgsub)              

            #opencv doesn't show the frame unless the following line is added
            key = cv2.waitKey(33) & 0xFF

            #if the 'q' key is pressed, break from the loop
            if key == ord("q"):
                break

        else:
            print 'invalid frame\n'
            break

    cv2. destroyAllWindows()

if __name__ == "__main__":
    main()
