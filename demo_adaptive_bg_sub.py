#import the necessary packages
import argparse
import datetime
import time
import cv2
import pdb
import numpy as np
from background_subtraction import background_subtractor

def createFrame(name, width, height, X, Y ):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)	
    cv2.resizeWindow(name, width, height)
    cv2.moveWindow(name, X, Y)

def main():
    #construct arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", help="Path to video file")
    args = vars(ap.parse_args())

    #get the video from the argument, and open it
    vid = args['video']
    print 'Vid path: ', vid

    if vid is None:
        cap = cv2.VideoCapture(0)
        time.sleep(0.25)
        print 'No video found'
    else:
        cap = cv2.VideoCapture(vid)

    #create display windows
    createFrame("Original", 500, 500, 50, 10)
    createFrame("Background Sub", 500, 500, 600, 10)
    createFrame("Contour", 500, 500, 1125,35)
    createFrame("Boundingbox", 500, 500, 50, 600)
    createFrame("Tracking", 500, 500, 600, 600)
    

    #holds all the tracking dots
    circle_pos = []

    #create an instance of the MOG2 background subtractor
    #bg_mog2 = cv2.createBackgroundSubtractorMOG2(detectShadows=False)
    bg_mog2 = background_subtractor()

    while True:
        (status, frame) = cap.read()
        
        if status == True:
            frame_mog2 = bg_mog2.removeBackground(frame)

            im2, contours, hierarchy = cv2.findContours(frame_mog2.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            #create two more frames for display purposes
            frame_rect = frame.copy()
            frame_tracking = frame.copy()

            #ignore contours with small areas
            contours_filtered = []
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    contours_filtered.append(contour)
            
            #create a contoured frame
            black_frame =  np.zeros((400,600,3), np.uint8)           
            cv2.drawContours(black_frame, contours_filtered, -1, (0,255,0), 3)           

            for contour in contours_filtered:
                (x,y,w,h) = cv2.boundingRect(contour)
                cv2.rectangle(frame_rect, (x,y), (x+w, y+h), (0,255,0),2)

                #draw tracking line
                x_pos = int(x + 0.5*w)
                y_pos = int(y + 0.5*h)
                circle_pos.append((x_pos,y_pos))

                for pos in circle_pos:
                    cv2.circle(frame_tracking, (pos[0],pos[1]),5,(0,255,0), thickness = -1)

            #Draw the frames
            cv2.imshow("Original", frame)
            cv2.imshow("Background Sub", frame_mog2)
            cv2.imshow("Contour", black_frame)
            cv2.imshow("Boundingbox", frame_rect)
            cv2.imshow("Tracking", frame_tracking)            

            #opencv doesn't show the frame unless the following line is added
            key = cv2.waitKey(33) & 0xFF

            #if the 'q' key is pressed, break from the loop
            if key == ord("q"):
                break

        else:
            print 'invalid frame\n'
            break

    cv2. destroyAllWindows()

if __name__ == "__main__":
    main()
