#import the necessary packages
import argparse
import datetime
import time
import cv2
import pdb

#construct arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="Path to video file")
args = vars(ap.parse_args())

#get the video from the argument, and open it
vid = args['video']
print 'Vid path: ', vid

if vid is None:
    cap = cv2.VideoCapture(0)
    time.sleep(0.25)
    print 'No video found'
else:
    cap = cv2.VideoCapture(vid)

#Create the windows and size them properly
cv2.startWindowThread()
cv2.namedWindow("FrameData", cv2.WINDOW_NORMAL)
cv2.namedWindow("GrayData", cv2.WINDOW_NORMAL)
cv2.namedWindow("BlurredData", cv2.WINDOW_NORMAL)
cv2.namedWindow("Boundingbox", cv2.WINDOW_NORMAL)
cv2.namedWindow("Tracking", cv2.WINDOW_NORMAL)

cv2.resizeWindow("FrameData", 500, 500)
cv2.resizeWindow("GrayData", 500, 500)
cv2.resizeWindow("BlurredData", 500, 500)
cv2.resizeWindow("Bouningbox", 500,500)
cv2.resizeWindow("Tracking", 500, 500)

cv2.moveWindow("FrameData", 50, 10)
cv2.moveWindow("GrayData", 600, 10)
cv2.moveWindow("BlurredData", 1125, 35)
cv2.moveWindow("Boundingbox", 50, 600)
cv2.moveWindow("Tracking", 600, 600)

first_frame = None

circle_pos = []
while True:
    (status, frame) = cap.read()
    
    if status == True:
               
        #convert to gray scale
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        #use gaussian blur to remove high frequency noise
        gray_frame_blurred = cv2.GaussianBlur(gray_frame, (21,21),0)
        
        #set the first frame if it is empty
        if first_frame == None:
            first_frame = gray_frame_blurred
            continue

        #subtract the background from the frame
        frame_nobg = cv2.absdiff(first_frame, gray_frame_blurred)
        frame_threshold = cv2.threshold(frame_nobg, 25, 255, cv2.THRESH_BINARY)[1]        

        frame_dilated = cv2.dilate(frame_threshold, None, iterations = 10)

        im2, contours, hierarchy = cv2.findContours(frame_dilated.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        frame_rect = frame.copy()
        frame_tracking = frame.copy()
        #ignore contours with small areas
        for contour in contours:
            if cv2.contourArea(contour) < 500:
                continue

            (x,y,w,h) = cv2.boundingRect(contour)
            cv2.rectangle(frame_rect, (x,y), (x+w, y+h), (0,255,0),2)

            #draw tracking line
            x_pos = int(x + 0.5*w)
            y_pos = int(y + 0.5*h)
            circle_pos.append((x_pos,y_pos))

            for pos in circle_pos:
                cv2.circle(frame_tracking, (pos[0],pos[1]),5,(0,255,0), thickness = -1)


        #Draw the frames
        cv2.imshow("FrameData", frame)
        cv2.imshow("GrayData", frame_threshold)
        cv2.imshow("BlurredData", frame_dilated)
        cv2.imshow("Boundingbox", frame_rect)
        cv2.imshow("Tracking", frame_tracking)

        key = cv2.waitKey(33) & 0xFF

        #if the 'q' key is pressed, break from the loop
        if key == ord("q"):
            break

    else:
        print 'invalid frame\n'
        break

cv2. destroyAllWindows()
