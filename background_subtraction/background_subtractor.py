#import the necessary packages
import cv2
import pdb
import numpy as np

class background_subtractor:

    def __init__(self):
        self.subtractor = cv2.createBackgroundSubtractorMOG2(detectShadows=False)
    
    def removeBackground(self, img, threshold = 25, thresh_color= 255, mode = cv2.THRESH_BINARY):
        '''Remove the background in the image using the backgroundsubtractorMOG2 method'''
        #see http://docs.opencv.org/master/db/d5c/tutorial_py_bg_subtraction.html#gsc.tab=0
        #threshold: see doc for cv2.threshold
        #thresh_color: see doc for cv2.threshold
        #mode: this paramter decides what mode of thresholding to apply
        
        frame_mog2 = self.subtractor.apply(img)
        frame_threshold = cv2.threshold(frame_mog2, threshold, thresh_color, mode)[1]
        return frame_threshold 
    

