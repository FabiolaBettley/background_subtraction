#import the necessary packages
import argparse
import datetime
import time
import cv2
import pdb
import numpy as np

#construct arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="Path to video file")
args = vars(ap.parse_args())

#get the video from the argument, and open it
vid = args['video']
print 'Vid path: ', vid

if vid is None:
    cap = cv2.VideoCapture(0)
    time.sleep(0.25)
    print 'No video found'
else:
    cap = cv2.VideoCapture(vid)

#Create the windows and size them properly
cv2.startWindowThread()
cv2.namedWindow("Original", cv2.WINDOW_NORMAL)
cv2.namedWindow("Absolute Difference", cv2.WINDOW_NORMAL)
cv2.namedWindow("Blurred", cv2.WINDOW_NORMAL)
cv2.namedWindow("MOG2", cv2.WINDOW_NORMAL)
cv2.namedWindow("MOG2_thresh", cv2.WINDOW_NORMAL)

cv2.resizeWindow("Original", 500, 500)
cv2.resizeWindow("Absolute Difference", 500, 500)
cv2.resizeWindow("Blurred", 500, 500)
cv2.resizeWindow("MOG2", 500, 500)
cv2.resizeWindow("MOG2_thresh", 500, 500)


cv2.moveWindow("Original", 50,10)
cv2.moveWindow("Absolute Difference", 600, 10)
cv2.moveWindow("Blurred", 1125, 35)
cv2.moveWindow("MOG2", 50, 600)
cv2.moveWindow("MOG2_thresh", 600, 600 )

first_frame_gray = None
circle_pos = []
bg_mog2 = cv2.createBackgroundSubtractorMOG2(detectShadows=True)

while True:
    (status, frame) = cap.read()

    if status == True:
               
        #convert to gray scale
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        #use gaussian blur to remove high frequency noise
        gray_frame_blurred = cv2.GaussianBlur(gray_frame, (21,21),0)
        
        #set the first frame if it is empty
        if first_frame_gray == None:
            first_frame_gray = gray_frame
            first_frame_blurred = gray_frame_blurred
            first_frame_raw = frame
            continue

        #subtract the background from the frame
        frame_diff_raw = cv2.absdiff(first_frame_raw, frame)
        frame_diff_gray = cv2.absdiff(first_frame_gray, gray_frame)
        frame_diff_blurred = cv2.absdiff(first_frame_blurred, gray_frame_blurred)
        frame_mog2 = bg_mog2.apply(frame)
        frame_mog2_thresh = cv2.threshold(frame_mog2, 25, 255, cv2.THRESH_BINARY)[1]
        im2, contours, hierarchy = cv2.findContours(frame_mog2_thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contours_filtered = []
        for contour in contours:
            if cv2.contourArea(contour) > 500:
                contours_filtered.append(contour)

        black_frame = np.zeros((400,600,3), np.uint8)
        cv2.drawContours(black_frame, contours_filtered, -1, (0,255,0), 3)

        #Draw the framesd
        cv2.imshow("Original", frame)
        cv2.imshow("Absolute Difference", frame_diff_raw)
        cv2.imshow("Blurred", frame_diff_blurred)
        cv2.imshow("MOG2", frame_mog2)
        cv2.imshow("MOG2_thresh", black_frame )

        key = cv2.waitKey(33) & 0xFF

        #if the 'q' key is pressed, break from the loop
        if key == ord("q"):
            break

    else:
        print 'invalid frame\n'
        break

cv2. destroyAllWindows()
